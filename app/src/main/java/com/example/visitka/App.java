package com.example.visitka;

import android.app.Application;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferencesStorage.getInstance(this);
    }
}
