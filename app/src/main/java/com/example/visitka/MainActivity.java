package com.example.visitka;

import android.graphics.Bitmap;

import com.example.visitka.databinding.ActivityMainBinding;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements MainContract.View {
private MainContract.Presenter presenter;



    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
    presenter = new MainPresenter();
    getBinding().setEvent(presenter);
    }

    @Override
    protected void onStartView() {
        presenter.startView(this);
    }

    @Override
    protected void onDestroyView() {
        presenter = null;
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public String outPutMethod() {
        return getBinding().etTextEmail1.getText().toString();
    }

    @Override
    public String outPutSecondMethod() {
        return getBinding().etTextEmail2.getText().toString();
    }

    @Override
    public String outPutThirddMethod() {
        return getBinding().etTextEmail3.getText().toString();
    }


    @Override
    public void message(String message) {

    }

    @Override
    public void setInput(Bitmap bitmap) {
        getBinding().imageViewQr.setImageBitmap(bitmap);
    }

    @Override
    public void setWhitePage() {

        getBinding().imageViewQr.setImageDrawable(getResources().getDrawable(R.drawable.white,null));
    }


}
