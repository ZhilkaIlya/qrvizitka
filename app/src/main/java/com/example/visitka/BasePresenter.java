package com.example.visitka;

public interface BasePresenter<V> {
    void startView(V view);

    void detachView();
}
