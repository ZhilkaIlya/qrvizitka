package com.example.visitka;

import android.graphics.Bitmap;

public interface MainContract {
    interface View{

    String outPutMethod();

        String outPutSecondMethod();

        String outPutThirddMethod();


        void message(String message);

        void setInput(Bitmap bitmap);

        void setWhitePage();

    }
    interface Presenter extends BasePresenter<View>{
        void saveData();

        void saveDataSecondLine();

        void saveDataThirdLine();

        void outPutQr_1();

        void outPutQr_2();

        void outPutQr_3();

        void cleanAll();


    }
}
