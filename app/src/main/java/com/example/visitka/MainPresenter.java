package com.example.visitka;

import android.graphics.Bitmap;

import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import timber.log.Timber;

public class MainPresenter implements MainContract.Presenter {

   private MainContract.View view;




    @Override
    public void startView(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        if (view != null) view = null;
    }

    @Override
    public void saveData() {
        String str = view.outPutMethod();
        if (str.length() ==0){
            str = view.outPutMethod();
        }
        SharedPreferencesStorage.getInstance().saveDataString(Constant.KEY_NAME,str);
        //String str1 =SharedPreferencesStorage.getInstance().loadDataString(Constant.KEY_NAME);
    }

    @Override
    public void saveDataSecondLine() {
        String str = view.outPutSecondMethod();
        if (str.length() ==0){
            str = view.outPutMethod();
        }
        SharedPreferencesStorage.getInstance().saveDataString(Constant.KEY_SECOND_NAME,str);
    }

    @Override
    public void saveDataThirdLine() {
        String str = view.outPutThirddMethod();
        if (str.length() ==0){
            str = view.outPutMethod();
        }
        SharedPreferencesStorage.getInstance().saveDataString(Constant.KEY_THIRD_NAME,str);
    }

    @Override
    public void outPutQr_1() {
        if (SharedPreferencesStorage.getInstance().loadDataString(Constant.KEY_NAME).length() == 0){
            view.setWhitePage();
        }
        qrCode(SharedPreferencesStorage.getInstance().loadDataString(Constant.KEY_NAME));
    }

    @Override
    public void outPutQr_2() {
        if (SharedPreferencesStorage.getInstance().loadDataString(Constant.KEY_SECOND_NAME).length() == 0){
            view.setWhitePage();
        }
        qrCode(SharedPreferencesStorage.getInstance().loadDataString(Constant.KEY_SECOND_NAME));

    }

    @Override
    public void outPutQr_3() {
        if (SharedPreferencesStorage.getInstance().loadDataString(Constant.KEY_THIRD_NAME).length() == 0){
            view.setWhitePage();
        }
        qrCode(SharedPreferencesStorage.getInstance().loadDataString(Constant.KEY_THIRD_NAME));
    }

    @Override
    public void cleanAll() {
        SharedPreferencesStorage.getInstance().clearAll();
        view.setWhitePage();
    }


    private void qrCode(String obj) {
        try {
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.encodeBitmap(obj, BarcodeFormat.QR_CODE, 800, 800);
            view.setInput(bitmap);
        } catch (Exception e) {
            view.message(e.getMessage());
            Timber.e(e);
        }
    }


}





